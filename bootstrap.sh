#!/bin/sh

SCRIPT_DIR=$(readlink -f $(dirname $0))
. "${SCRIPT_DIR}/functions.sh"
. "${SCRIPT_DIR}/config.sh"
. "${SCRIPT_DIR}/python.version"

check_app patch
check_app tar
check_app wget
check_app git
check_app make

if [ ! -f "${SCRIPT_DIR}/Python-${PYTHON_VERSION}.tgz" ]; then
    info "Getting Python-${PYTHON_VERSION}"
    
    cd ${SCRIPT_DIR}
    download "http://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz"
fi

if [ ! -d "${SCRIPT_DIR}/Python-host" ]; then
    cd ${SCRIPT_DIR}
    unpack "${SCRIPT_DIR}/Python-${PYTHON_VERSION}.tgz"
    mv "Python-${PYTHON_VERSION}" "Python-host"
fi

# Python
if [ ! -d "${SCRIPT_DIR}/Python" ]; then
    cd ${SCRIPT_DIR}
    unpack "${SCRIPT_DIR}/Python-${PYTHON_VERSION}.tgz"
    mv "Python-${PYTHON_VERSION}" "Python"
    
    cd "${SCRIPT_DIR}/Python"
    info "Patching python"
    
    patch_all "${SCRIPT_DIR}/patch/python" "Python-${PYTHON_VERSION}*.patch"
fi

# Host Python
if [ ! -d "${SCRIPT_DIR}/hostpython" -o ! -e "${SCRIPT_DIR}/hostpython/bin/hostpython" -o ! -e "${SCRIPT_DIR}/hostpython/bin/hostpgen" ]; then
    info "Building python host"
    
    cd "${SCRIPT_DIR}/Python-host"
    ./configure --prefix="${SCRIPT_DIR}/hostpython"
    make -j4
    make install
    mv python "${SCRIPT_DIR}/hostpython/bin/hostpython"
    mv Parser/pgen "${SCRIPT_DIR}/hostpython/bin/hostpgen"
    make distclean
fi

# Openssl
if [ ! -d "${SCRIPT_DIR}/openssl" ]; then
    info "Getting openssl"

    cd "${SCRIPT_DIR}"
    git clone https://iusvyryd@bitbucket.org/iusvyryd/openssl_androidbuild.git openssl
fi

# Sqlite
if [ ! -d "${SCRIPT_DIR}/sqlite" ]; then
    info "Getting sqlite"

    cd "${SCRIPT_DIR}"
    git clone https://iusvyryd@bitbucket.org/iusvyryd/sqlite_androidbuild.git sqlite
    cd "${SCRIPT_DIR}/sqlite"
    git checkout tags/v3.8.1
fi

# PyCrypto
if [ ! -d "${SCRIPT_DIR}/pycrypto" ]; then
    cd "${SCRIPT_DIR}"

    if [ ! -f "${SCRIPT_DIR}/pycrypto-2.6.tar.gz" ]; then
        info "Getting pycrypto"
        download "http://pypi.python.org/packages/source/p/pycrypto/pycrypto-2.6.tar.gz"
    fi

    unpack "${SCRIPT_DIR}/pycrypto-2.6.tar.gz"
    mv "pycrypto-2.6" "pycrypto"

    info "Patching pycrypto"

    cd "${SCRIPT_DIR}/pycrypto"
    patch_all "${SCRIPT_DIR}/patch/pycrypto" "pycrypto-2.6*.patch"
fi

# PsUtil
if [ ! -d "${SCRIPT_DIR}/psutil" ]; then
    cd "${SCRIPT_DIR}"

    if [ ! -f "${SCRIPT_DIR}/psutil-0.6.1.tar.gz" ]; then
        info "Getting psutil"
        download "http://psutil.googlecode.com/files/psutil-0.6.1.tar.gz"
    fi

    unpack "${SCRIPT_DIR}/psutil-0.6.1.tar.gz"
    mv "psutil-0.6.1" "psutil"

    info "Patching psutil"

    cd "${SCRIPT_DIR}/psutil"
    patch_all "${SCRIPT_DIR}/patch/psutil" "psutil-0.6.1*.patch"
fi

# Apsw
if [ ! -d "${SCRIPT_DIR}/apsw" ]; then
    cd "${SCRIPT_DIR}"
    . "${SCRIPT_DIR}/sqlite/sqlite.version"
    info "Sqlite version ${SQLITE_VERSION}"

    if [ ! -f "${SCRIPT_DIR}/apsw-${SQLITE_VERSION}-r1.zip" ]; then
        info "Getting apsw"
        download "https://apsw.googlecode.com/files/apsw-${SQLITE_VERSION}-r1.zip"
    fi

    unpack "${SCRIPT_DIR}/apsw-${SQLITE_VERSION}-r1.zip"
    mv "apsw-${SQLITE_VERSION}-r1" "apsw"

    info "Patching apsw"

    cd "${SCRIPT_DIR}/apsw"
    patch_all "${SCRIPT_DIR}/patch/apsw" "apsw-${SQLITE_VERSION}*.patch"
fi

# bitarray
if [ ! -d "${SCRIPT_DIR}/bitarray" ]; then
    cd "${SCRIPT_DIR}"

    if [ ! -f "${SCRIPT_DIR}/bitarray-0.8.1.tar.gz" ]; then
        info "Getting bitarray"
        download "https://pypi.python.org/packages/source/b/bitarray/bitarray-0.8.1.tar.gz"
    fi

    unpack "${SCRIPT_DIR}/bitarray-0.8.1.tar.gz"
    mv "bitarray-0.8.1" "bitarray"

    info "Patching bitarray"

    cd "${SCRIPT_DIR}/bitarray"
    patch_all "${SCRIPT_DIR}/patch/bitarray" "bitarray-0.8.1*.patch"
fi

# blist
if [ ! -d "${SCRIPT_DIR}/blist" ]; then
    cd "${SCRIPT_DIR}"

    if [ ! -f "${SCRIPT_DIR}/blist-1.3.4.tar.gz" ]; then
        info "Getting blist"
        download "https://pypi.python.org/packages/source/b/blist/blist-1.3.4.tar.gz"
    fi

    unpack "${SCRIPT_DIR}/blist-1.3.4.tar.gz"
    mv "blist-1.3.4" "blist"

    info "Patching blist"

    cd "${SCRIPT_DIR}/blist"
    patch_all "${SCRIPT_DIR}/patch/blist" "blist-1.3.4*.patch"
fi




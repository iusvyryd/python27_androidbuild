#!/bin/sh

SCRIPT_DIR=$(readlink -f $(dirname $0))
. "${SCRIPT_DIR}/functions.sh"
. "${SCRIPT_DIR}/config.sh"

if [ "x${NDK}" != "x" ]; then
    check_path ${NDK}
    ANDROID_NDK=${NDK}
else
    if [ "x${ANDROID_NDK}" != "x" ]; then
        check_path ${ANDROID_NDK}
    else
        error "Specify path to android ndk as NDK"
    fi
fi

if [ "x${ABI}" != "x" ]; then
    check_abi ${ABI}
    ANDROID_ABI=${ABI}
else
    if [ "x${ANDROID_ABI}" != "x" ]; then
        check_abi "${ANDROID_ABI}"
    else
        error "Specify android abi"
    fi
fi

check_app uname

CFLAGS="-mandroid -ffunction-sections -funwind-tables -fomit-frame-pointer -no-canonical-prefixes"
EXTRA_CFLAGS="-DNDEBUG -DNO_MALLINFO=1 -I${SCRIPT_DIR}/include"
OFLAGS=""
NDK_PLATFORM_PATH=""
HOSTARCH=""
PATH_HOST=""
CCPREFIX=""
case ${ANDROID_ABI} in
    "armeabi-v7a")
        CFLAGS="${CFLAGS} -march=armv7-a -mfloat-abi=softfp -mfpu=vfpv3-d16 -mthumb -fpic"
        OFLAGS="-Os"
        HOSTARCH="arm-linux"
        PATH_HOST="arm-linux-androideabi"
        CCPREFIX="arm-linux-androideabi"
        NDK_PLATFORM_PATH="${ANDROID_NDK}/platforms/android-${ANDROID_PLATFORM_VERSION}/arch-arm"
    ;;
    "armeabi")
        CFLAGS="${CFLAGS} -mthumb -fpic"
        OFLAGS="-Os"
        HOSTARCH="arm-linux"
        PATH_HOST="arm-linux-androideabi"
        CCPREFIX="arm-linux-androideabi"
        NDK_PLATFORM_PATH="${ANDROID_NDK}/platforms/android-${ANDROID_PLATFORM_VERSION}/arch-arm"
    ;;
    "x86")
        CFLAGS="${CFLAGS} -march=pentium -frtti -fexceptions"
        OFLAGS="-O2"
        HOSTARCH="i686-linux"
        PATH_HOST="x86"
        CCPREFIX="i686-linux-android"
        NDK_PLATFORM_PATH="${ANDROID_NDK}/platforms/android-${ANDROID_PLATFORM_VERSION}/arch-x86"
    ;;
    "mips")
        CFLAGS="${CFLAGS} -march=mips32 -mtune=mips32r2 -mhard-float -fpic"
        OFLAGS="-O2"
        HOSTARCH="mips-linux"
        PATH_HOST="mipsel-linux-android"
        CCPREFIX="mipsel-linux-android"
        NDK_PLATFORM_PATH="${ANDROID_NDK}/platforms/android-${ANDROID_PLATFORM_VERSION}/arch-mips"
    ;;
esac

TOOLCHAIN_PATH=`echo ${ANDROID_NDK}/toolchains/${PATH_HOST}-${GCC_VERSION}/prebuilt/\`uname|tr A-Z a-z\`-*/bin`
check_path ${NDK_PLATFORM_PATH}
check_path ${TOOLCHAIN_PATH}

export CFLAGS="--sysroot ${NDK_PLATFORM_PATH} ${CFLAGS} ${OFLAGS} ${EXTRA_CFLAGS}"
export CXXFLAGS="${CFLAGS}"
export CC="${CCPREFIX}-gcc ${CFLAGS}"
export CXX="${CCPREFIX}-g++ ${CXXFLAGS}"
export AR="${CCPREFIX}-ar"
export RANLIB="${CCPREFIX}-ranlib"
export STRIP="${CCPREFIX}-strip --strip-unneeded"
export MAKE="make"
export PATH="${ANDROID_NDK}:${TOOLCHAIN_PATH}:${PATH}"

build_openssl()
{
    cd ${SCRIPT_DIR}/openssl
    NDK=${ANDROID_NDK} ABI=${ANDROID_ABI} sh build.sh
    cp ${SCRIPT_DIR}/openssl/libs/${ANDROID_ABI}/*.so ${SCRIPT_DIR}/lib
    cp -r ${SCRIPT_DIR}/openssl/include/* ${SCRIPT_DIR}/include
}

build_sqlite()
{
    cd ${SCRIPT_DIR}/sqlite
    NDK=${ANDROID_NDK} ABI=${ANDROID_ABI} sh build.sh
    cp ${SCRIPT_DIR}/sqlite/libs/${ANDROID_ABI}/* ${SCRIPT_DIR}/lib
    cp ${SCRIPT_DIR}/sqlite/src/*.h ${SCRIPT_DIR}/include
}

build_crypt()
{
    cd ${SCRIPT_DIR}/libcrypt
    NDK=${ANDROID_NDK} ABI=${ANDROID_ABI} sh build.sh
    cp ${SCRIPT_DIR}/libcrypt/libs/${ANDROID_ABI}/* ${SCRIPT_DIR}/lib
    cp ${SCRIPT_DIR}/libcrypt/*.h ${SCRIPT_DIR}/include
}

build_libpython()
{
    cd ${SCRIPT_DIR}/Python

    $MAKE distclean

    ./configure --host=${HOSTARCH} --build=i686-linux-gnu \
        --enable-ipv6 --enable-shared
    cat pyconfig.h \
    | sed -e '/HAVE_FDATASYNC/ c#undef HAVE_FDATASYNC' \
    | sed -e '/HAVE_KILLPG/ c#undef HAVE_KILLPG' \
    | sed -e '/HAVE_GETHOSTBYNAME_R/ c#undef HAVE_GETHOSTBYNAME_R' \
    | sed -e '/HAVE_DECL_ISFINITE/ c#undef HAVE_DECL_ISFINITE' \
    > temp
    mv temp pyconfig.h
    
    BLDSHARED="${CCPREFIX}-gcc -shared ${CFLAGS}"
    $MAKE -j4 HOSTPYTHON="${HOSTPYTHON}" HOSTPGEN="${HOSTPGEN}" BLDSHARED="${BLDSHARED}" \
    CROSS_COMPILE_TARGET=yes HOSTARCH="${HOSTARCH}" NDKPLATFORM="${NDK_PLATFORM_PATH}" \
    BUILDARCH=i686-linux-gnu INSTSONAME=libpython2.7.so libpython2.7.so
}

build_python() 
{
    cd ${SCRIPT_DIR}/Python

    $MAKE distclean

    ./configure --host=${HOSTARCH} --build=i686-linux-gnu \
        --enable-ipv6 --enable-shared
    cat pyconfig.h \
    | sed -e '/HAVE_FDATASYNC/ c#undef HAVE_FDATASYNC' \
    | sed -e '/HAVE_KILLPG/ c#undef HAVE_KILLPG' \
    | sed -e '/HAVE_GETHOSTBYNAME_R/ c#undef HAVE_GETHOSTBYNAME_R' \
    | sed -e '/HAVE_DECL_ISFINITE/ c#undef HAVE_DECL_ISFINITE' \
    > temp
    mv temp pyconfig.h
    
    BLDSHARED="${CCPREFIX}-gcc -shared ${CFLAGS} -L${SCRIPT_DIR}/lib -lpython2.7 -lm -Wl,--no-undefined"
    $MAKE -j4 HOSTPYTHON="${HOSTPYTHON}" HOSTPGEN="${HOSTPGEN}" BLDSHARED="${BLDSHARED}" \
    CROSS_COMPILE_TARGET=yes HOSTARCH="${HOSTARCH}" NDKPLATFORM="${NDK_PLATFORM_PATH}" \
    BUILDARCH=i686-linux-gnu INSTSONAME=libpython2.7.so

    $MAKE -j4 install HOSTPYTHON="${HOSTPYTHON}" BLDSHARED="${BLDSHARED}" \
    prefix=${SCRIPT_DIR}/buildpython \
    NDKPLATFORM="${NDK_PLATFORM_PATH}" CROSS_COMPILE_TARGET=yes INSTSONAME=libpython2.7.so
}

build_pycrypto()
{
    cd ${SCRIPT_DIR}/pycrypto
    rm -r ${SCRIPT_DIR}/pycrypto/build
    export ac_cv_func_malloc_0_nonnull=yes
    export ARM_LINKER="${CCPREFIX}-gcc"
    
    ./configure --host=${HOSTARCH} --enable-shared
    BLDSHARED="${CCPREFIX}-gcc -shared ${CFLAGS} -L${SCRIPT_DIR}/lib -lpython2.7 -lm -Wl,--no-undefined" \
    ${HOSTPYTHON} setup.py build
}

build_psutil()
{
    cd ${SCRIPT_DIR}/psutil
    rm -r ${SCRIPT_DIR}/psutil/build
    export ARM_LINKER="${CCPREFIX}-gcc"
    
    BLDSHARED="${CCPREFIX}-gcc -shared ${CFLAGS} -L${SCRIPT_DIR}/lib -lpython2.7 -lm -Wl,--no-undefined" \
    ${HOSTPYTHON} setup.py build
}

build_apsw()
{
    cd ${SCRIPT_DIR}/apsw
    rm -r ${SCRIPT_DIR}/apsw/build
    export ARM_LINKER="${CCPREFIX}-gcc"
    
    BLDSHARED="${CCPREFIX}-gcc -shared ${CFLAGS} -L${SCRIPT_DIR}/lib -lpython2.7 -lsqlite3 -lm -Wl,--no-undefined" \
    ${HOSTPYTHON} setup.py build
}

build_bitarray()
{
    cd ${SCRIPT_DIR}/bitarray
    rm -r ${SCRIPT_DIR}/bitarray/build
    export ac_cv_func_malloc_0_nonnull=yes
    export ARM_LINKER="${CCPREFIX}-gcc"
    
    BLDSHARED="${CCPREFIX}-gcc -shared ${CFLAGS} -L${SCRIPT_DIR}/lib -lpython2.7 -lm -Wl,--no-undefined" \
    ${HOSTPYTHON} setup.py build
}

build_blist()
{
    cd ${SCRIPT_DIR}/blist
    rm -r ${SCRIPT_DIR}/blist/build
    export ac_cv_func_malloc_0_nonnull=yes
    export ARM_LINKER="${CCPREFIX}-gcc"
    
    BLDSHARED="${CCPREFIX}-gcc -shared ${CFLAGS} -L${SCRIPT_DIR}/lib -lpython2.7 -lm -Wl,--no-undefined" \
    ${HOSTPYTHON} setup.py build
}

build_miniupnp()
{
    cd ${SCRIPT_DIR}/miniupnp
    rm -r ${SCRIPT_DIR}/miniupnp/build
    rm -r ${SCRIPT_DIR}/miniupnp/obj
    NDK=${ANDROID_NDK} ABI=${ANDROID_ABI} sh build.sh
    cp -f ${SCRIPT_DIR}/miniupnp/obj/local/${ANDROID_ABI}/libminiupnpc.a ${SCRIPT_DIR}/miniupnp
    
    export ac_cv_func_malloc_0_nonnull=yes
    export ARM_LINKER="${CCPREFIX}-gcc"
    BLDSHARED="${CCPREFIX}-gcc -shared ${CFLAGS} -L${SCRIPT_DIR}/lib -lpython2.7 -lm -Wl,--no-undefined" \
    ${HOSTPYTHON} setup.py build
}

HOSTPYTHON=${SCRIPT_DIR}/hostpython/bin/hostpython
HOSTPGEN=${SCRIPT_DIR}/hostpython/bin/hostpgen

info "NDK in ${ANDROID_NDK}"
info "Building for ${ANDROID_ABI}"
info "PATH=${PATH}"

rm -r -f ${SCRIPT_DIR}/buildpython
rm -r -f ${SCRIPT_DIR}/include
rm -r -f ${SCRIPT_DIR}/lib

mkdir -p ${SCRIPT_DIR}/include
mkdir -p ${SCRIPT_DIR}/lib

build_openssl
build_sqlite
build_crypt

# python
rm -r -f ${SCRIPT_DIR}/buildpython
build_libpython
mv ${SCRIPT_DIR}/Python/libpython2.7.so ${SCRIPT_DIR}/lib
build_python

# pycrypto
build_pycrypto

# psutil
build_psutil

# apsw
build_apsw

# bitarray
build_bitarray

# blist
build_blist

# miniupnp
build_miniupnp

# move all in build python
cd ${SCRIPT_DIR}
rm -f ${SCRIPT_DIR}/buildpython/lib/libpython2.7.so
cp -r ${SCRIPT_DIR}/include/* ${SCRIPT_DIR}/buildpython/include
cp ${SCRIPT_DIR}/lib/* ${SCRIPT_DIR}/buildpython/lib
check_path ${SCRIPT_DIR}/buildpython/lib/python2.7/lib-dynload
cp -r ${SCRIPT_DIR}/pycrypto/build/lib.*/Crypto ${SCRIPT_DIR}/buildpython/lib/python2.7/lib-dynload/
cp -r ${SCRIPT_DIR}/psutil/build/lib.*/psutil ${SCRIPT_DIR}/buildpython/lib/python2.7/lib-dynload/
cp ${SCRIPT_DIR}/psutil/build/lib.*/*.so ${SCRIPT_DIR}/buildpython/lib/python2.7/lib-dynload/psutil/
cp ${SCRIPT_DIR}/apsw/build/lib.*/*.so ${SCRIPT_DIR}/buildpython/lib/python2.7/lib-dynload/
cp -r ${SCRIPT_DIR}/bitarray/build/lib.*/bitarray ${SCRIPT_DIR}/buildpython/lib/python2.7/lib-dynload/
cp -r ${SCRIPT_DIR}/blist/build/lib.*/* ${SCRIPT_DIR}/buildpython/lib/python2.7/lib-dynload/
cp -r ${SCRIPT_DIR}/miniupnp/build/lib.*/* ${SCRIPT_DIR}/buildpython/lib/python2.7/lib-dynload/

if [ "x${PREFIX}" = "x" ]; then
    PREFIX="${SCRIPT_DIR}/python-${ANDROID_ABI}-build"
fi

rm -r -f ${PREFIX}
mv ${SCRIPT_DIR}/buildpython ${PREFIX}
info "Well done ${PREFIX}"
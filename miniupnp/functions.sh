
info()
{
    echo "\033[33mInfo:\033[0m $1"
}

error()
{
    echo "\033[31mError:\033[0m $1"
    exit 1
}

check_abi()
{
    if [ "$1" = "armeabi-v7a" -o "$1" = "armeabi" -o "$1" = "x86" -o "$1" = "mips" -o "$1" = "all" ]; then
        ABI="$1"
    else
        error "Unknown abi $1"
    fi
}

check_path()
{
    test ! -d $1 && error "Path $1 not exists"
}
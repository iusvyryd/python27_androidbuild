LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := miniupnp
LOCAL_MODULE_FILENAME := libminiupnpc
LOCAL_SRC_FILES := \
    igd_desc_parse.c \
    miniupnpc.c \
    minixml.c \
    minisoap.c \
    miniwget.c \
    upnpc.c \
    upnpcommands.c \
    upnpreplyparse.c \
    testminixml.c \
    minixmlvalid.c \
    testupnpreplyparse.c \
    minissdpc.c \
    upnperrors.c \
    testigddescparse.c \
    testminiwget.c \
    connecthostport.c \
    portlistingparse.c \
    receivedata.c
LOCAL_CFLAGS := -DMINIUPNPC_SET_SOCKET_TIMEOUT -DMINIUPNPC_GET_SRC_ADDR -D_BSD_SOURCE -D_POSIX_C_SOURCE=1
LOCAL_C_EXPORTS := $(LOCAL_PATH)

include $(BUILD_STATIC_LIBRARY)

python27-AndroidBuild
--------------------

Build of Python-2.7.x for android.
Additional modules:
* [pycrypto]-2.6
* [psutil]-0.6.1
* [apsw]-3.8.1
* [bitarray]-0.8.1
* [blist]-1.3.4
* [miniupnp]

Only for Linux hosts.

`cd python27_androidbuild`

`sh bootstrap.sh`

`NDK=/path/to/android/ndk sh build.sh`

to build for specific ABI (default ABI specified in config.sh) specify:

`ABI=armeabi-v7`

All default configuration values in config.sh.

set PREFIX to specify final destination

`PREFIX=/tmp/python27-armeabi-v7-build`

if PREFIX not set, binaries will be in *python-$ABI-build* directory

Links:

[https://code.google.com/p/android-python27/]

[https://code.google.com/p/android-scripting/]


[pycrypto]:https://www.dlitz.net/software/pycrypto/
[psutil]:https://code.google.com/p/psutil/
[apsw]:https://code.google.com/p/apsw/
[bitarray]:https://pypi.python.org/pypi/bitarray/
[blist]:https://pypi.python.org/pypi/blist/
[miniupnp]:https://github.com/miniupnp/miniupnp
[https://code.google.com/p/android-python27/]:https://code.google.com/p/android-python27/
[https://code.google.com/p/android-scripting/]:https://code.google.com/p/android-scripting/
info()
{
    echo "\033[33mInfo:\033[0m $1"
}

error()
{
    echo "\033[31mError:\033[0m $1"
    exit 1
}

check_abi()
{
    if [ "$1" = "armeabi-v7a" -o "$1" = "armeabi" -o "$1" = "x86" -o "$1" = "mips" -o "$1" = "all" ]; then
        ABI="$1"
    else
        error "Unknown abi $1"
    fi
}

check_path()
{
    test -d $1 || error "Path $1 not exists"
}

check_app()
{
    app=$(which $1 &> /dev/null) || error "$1 not found"
}

download()
{
    check_app "wget"
    wget $1
}

unpack()
{
    filename=$(basename "$1")
    extension="${filename##*.}"
    
    info "Unpacking $1"
    case ${extension} in
        tgz)
            check_app tar
            tar zxvf $1
        ;;
        gz)
            check_app tar
            tar zxvf $1
        ;;
        zip)
            check_app unzip
            unzip $1
        ;;
    esac
}

patch_all()
{
    check_path $1
    find $1 -type f -name $2 -exec patch -p1 -i {} \;
}